import React from 'react';
import axios from "axios";
import ReactDOM from 'react-dom';

import App from './App';

import './index.css';



axios.defaults.baseURL = 'https://restcountries.eu/';

ReactDOM.render(<App />, document.getElementById('root'));
