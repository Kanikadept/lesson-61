import React, {useState, useEffect} from 'react';

import './Country.css';
import ListOfCountries from "../ListOfCountries/ListOfCountries";
import CountryInfo from "../CountryInfo/CountryInfo";
import axios from "axios";

const Country = () => {

    const [countries, setCountries] = useState([]);

    const [selectedCountry, setSelectedCountry] = useState(null);

    const [countryInfo, setCountryInfo] = useState(null);

    useEffect(() => {

        const getData = async () => {
            const countriesResponse = await axios.get('rest/v2/all?fields=name%3Balpha3Code');
            setCountries(countriesResponse.data);
        }
        getData();

    }, [])

    useEffect(() => {

        if(selectedCountry === null) {
            return null;
        }

        const getData = async () => {
            const countryResponse = await axios.get('rest/v2/alpha/' + selectedCountry);

            const promises = countryResponse.data.borders.map(async border => {
                const response = await axios.get('rest/v2/alpha/' + border);
                return response.data.name;
            });

            const newBorders = await Promise.all(promises);

            countryResponse.data.borders = newBorders;

            setCountryInfo(countryResponse.data);
        }
        getData();

    }, [selectedCountry])

    const handleSelectCountry = (country) => {
        setSelectedCountry(country);
    }

    return (
        <div className="country">
            <ListOfCountries countries={countries} selectCountry={handleSelectCountry}/>
            <CountryInfo selectedCountry={countryInfo}/>
        </div>
    );
};

export default Country;